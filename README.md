# README #

BBC SWD Assignment.




### SET UP ###

1 - Install node modules
```shell
npm install
```

2 - Run front-end tasks
```shell
grunt
```

3 - Run JS Unit tests
```shell
grunt unit
```

4 - Run E2E tests
```shell
grunt e2e
```




### FRONT-END TECHONOLOGIES ###

- HTML5
- CSS3
- CSS Bootstrap
- Grunt
- SASS
- AngularJS
- Karma - Unit and E2E testing




### ANGULARJS APP ARCHITECTURE ###

This is an AngularJS app with the following structure:

![Alt text](https://dl.dropboxusercontent.com/u/1843460/diagram.jpg)





### WEB APP ###
After a successfull grunt build, the app will be live on:

```shell
http://localhost/bbc-media-project/dist/
```




### AUTHOR ###

Joao Cruz - Senior Web Developer

jlopes.cruz@gmail.com