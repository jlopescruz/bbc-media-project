app.controller('appController', ['$scope', '$window', 'mediaService', function($scope, $window, mediaService) {
    
    $scope.media                = [];
    $scope.isMediaLoading       = true;
    $scope.pageWidth            = 728;
    $scope.nrItemsPerPage       = 5;
    $scope.pageWrapperPosition  = 0;
    $scope.currentPage          = 0;

    mediaService.getData().then(function(data){
        $scope.isMediaLoading    = false;
        $scope.media             = data;
        $scope.nrPages           = Math.ceil(data.length / $scope.nrItemsPerPage);
    });

    $scope.moveToPage = function (index) {
    	$scope.currentPage         = index;
		$scope.pageWrapperPosition = -(index * $scope.pageWidth) + 'px';
    };

}]);