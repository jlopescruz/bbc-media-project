app.factory('mediaService', ['$http', '$q', function($http, $q) {

    var mediaService = {

        isMediaLoaded : false,
        media         : [],
        
        getData : function () {
            var deferred = $q.defer();

            if (mediaService.isMediaLoaded) {
                deferred.resolve(mediaService.media);
                
            } else {
                $http({
                    method: 'GET',
                    url: 'https://s3-eu-west-1.amazonaws.com/irfs-application/data.json'

                }).success(function(data, status, headers, config) {
                    mediaService.isMediaLoaded = true;
                    mediaService.media         = data;
                    deferred.resolve(mediaService.media);

                }).error(function(data, status) {
                    deferred.reject(status);

                });
            }

            return deferred.promise;
        }

    };

    return mediaService;
}]);