app.directive('mediaBlock', [function() {
    return {
        restrict: 'A',
        replace: true,
        scope: {
            item: '=',
            index: '='
        },
        isolated: true,
        templateUrl : 'partials/media-block.html',
        controller:   'mediaBlockController'
    };
}]);